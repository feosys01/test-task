<?php
namespace console\controllers;

use common\components\prises\PrizeType;
use common\models\mysql\UsersPrisesRecord;
use yii\console\Controller;

/**
 * Created by PhpStorm.
 * User: bohdan
 * Date: 01.12.18
 * Time: 15:16
 */

/**
 * Class PriseHandlerController
 */
class PrizeHandlerController extends Controller
{
    /** @var int  */
    const BATCH_SIZE = 100;

    /**
     * Send money
     */
    public function actionSendMoney()
    {
        $query = UsersPrisesRecord::find()->where(['prise_type' => PrizeType::TYPE_MONEY, 'is_processed' => 0]);

        foreach($query->batch(self::BATCH_SIZE) as $prises) {
            foreach ($prises as $item) {
                /** @var $item UsersPrisesRecord */

                // TODO send money

                $item->is_processed = 1;
                $item->save();
            }
        }
    }
}