<?php
namespace common\models\mysql;

/**
 * Created by PhpStorm.
 * User: bohdan
 * Date: 01.12.18
 * Time: 14:07
 */

use common\components\prizes\PrizeHelper;
use common\components\prizes\prizesTypes\MoneyType;
use common\components\prizes\PrizeType;
use common\models\User;
use yii\behaviors\TimestampBehavior;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;

/**
 * Class UsersPrizesRecord
 *
 * @property integer $user_id
 * @property integer $prize_type
 * @property integer $amount
 * @property integer $product_id
 * @property integer $is_processed
 * @property integer $created_at
 * @property integer $updated_at
 */
class UsersPrizesRecord extends ActiveRecord
{
    public static function tableName()
    {
        return 'users_prizes';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * @return array
     */
    public function rules ()
    {
        return [
            [['user_id', 'prize_type'], 'required'],
            [['amount', 'product_id', 'created_at', 'updated_at'], 'safe'],
            [['user_id', 'prize_type', 'amount', 'product_id', 'created_at', 'updated_at'], 'integer']
        ];
    }

    /**
     * @param PrizeType $prize
     * @param $userId
     * @return bool
     */
    public static function addPrize(PrizeType $prize, $userId)
    {
        $result = false;
        $userId = intval($userId);

        // user exist
        if (User::find()->where(['id' => $userId])->exists()) {
            $model = new self();
            $model->user_id = $userId;
            $model->prize_type = $prize->getType();

            // set value
            switch($prize->getType()) {
                case PrizeType::TYPE_MONEY:
                case PrizeType::TYPE_BONUSES:
                    $model->amount = $prize->getValue();
                    break;
                case PrizeType::TYPE_PRODUCTS:
                    $model->product_id = $prize->getValue();
                    break;
            }

            $result = $model->save();
        }

        return $result;
    }

    /**
     * @param $userId
     * @return UsersPrizesRecord|false
     */
    public static function getLastAddedPrize($userId)
    {
        $userId = intval($userId);

        // user exist
        if (User::find()->where(['id' => $userId])->exists()) {
            $lastAddedPrize = self::find()->where(['user_id' => $userId])->orderBy('id DESC')->one();

            if ($lastAddedPrize) {
                return $lastAddedPrize;
            }
        }

        return false;
    }

    /**
     * Get prize name
     */
    public function getPrizeName()
    {
        /** @var $prizeObj PrizeType */
        $prizeObj = PrizeHelper::getPrizeObject($this->prize_type);

        if ($prizeObj) {
            return $prizeObj->getName();
        }

        return null;
    }

    /**
     * Get prize name
     * @return string
     */
    public function getPrizeValueText()
    {
        $result = null;

        // set value
        switch($this->prize_type) {
            case PrizeType::TYPE_MONEY:
            case PrizeType::TYPE_BONUSES:
                $result = $this->amount;
                break;
            case PrizeType::TYPE_PRODUCTS:
                $productId = $this->product_id;
                $productModel = ProductsRecord::findOne(['id' => $productId]);
                if ($productModel) {
                    $result = $productModel->name;
                }
                break;
        }

        return $result;
    }

    /**
     * @return ActiveDataProvider
     */
    public function search()
    {
        $query = self::find()->where(['user_id' => \Yii::$app->user->id]);

        return new ActiveDataProvider([
            'query' => $query
        ]);
    }
}