<?php
namespace common\models\mysql;

/**
 * Created by PhpStorm.
 * User: bohdan
 * Date: 01.12.18
 * Time: 14:07
 */

use common\components\prises\PrizeType;
use common\models\User;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * Class UsersPrisesRecord
 *
 * @property-read integer $id
 * @property string $name
 * @property integer $available_count
 * @property integer $created_at
 * @property integer $updated_at
 */
class ProductsRecord extends ActiveRecord
{
    public static function tableName()
    {
        return 'products';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * @return array
     */
    public function rules ()
    {
        return [
            [['name'], 'required'],
            [['available_count', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @return array|ProductsRecord[]
     */
    public static function getAvailableProducts()
    {
        return self::find()->where(['>', 'available_count', 0])->all();
    }

    /**
     * @return bool
     */
    public static function getIsExistAvailableProducts()
    {
        return self::find()->where(['>', 'available_count', 0])->exists();
    }

    /**
     * Decrease count
     */
    public function decCount()
    {
        if ($this->available_count > 0) {
            $this->available_count--;
            $this->save();
        }
    }
}