<?php
namespace common\components\prizes;

use common\components\prizes\prizesTypes\BonusesType;
use common\components\prizes\prizesTypes\MoneyType;
use common\components\prizes\prizesTypes\ProductsType;

/**
 * Created by PhpStorm.
 * User: bohdan
 * Date: 01.12.18
 * Time: 13:41
 */

/**
 * Class PrizeHelper
 */
class PrizeHelper
{
    /** @var array */
    const PRIZES = [
        PrizeType::TYPE_MONEY => MoneyType::class,
        PrizeType::TYPE_BONUSES =>  BonusesType::class,
        PrizeType::TYPE_PRODUCTS =>  ProductsType::class
    ];

    /** @var int  */
    const SEARCH_ITERATION_LIMIT = 50;

    /**
     * Get random prize
     * @return PrizeType|false
     */
    public static function getRandomPrize()
    {
        $prizesClasses = self::PRIZES;

        for ($i = 0; $i < self::SEARCH_ITERATION_LIMIT; $i++) {
            /** @var $prizeObject PrizeType */

            // get random prize object
            $randKey = array_rand($prizesClasses);
            $prizeObject = new $prizesClasses[$randKey];

            // object is available
            if ($prizeObject->getIsAvailable()) {
                return $prizeObject;
            } else {
                unset($prizesClasses[$randKey]); // remove prize for current searching
            }

            if (empty($prizesClasses)) {
                return false;
            }
        }

        return false;
    }

    /**
     * Get prize type object by type id
     * @param $type
     * @return bool|mixed
     */
    public static function getPrizeObject($type)
    {
        $type = intval($type);

        if (isset(self::PRIZES[$type])) {
            $prizeClass = self::PRIZES[$type];
            return new $prizeClass;
        }

        return false;
    }
}