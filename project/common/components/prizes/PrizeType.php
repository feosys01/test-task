<?php
namespace common\components\prizes;

/**
 * Created by PhpStorm.
 * User: bohdan
 * Date: 01.12.18
 * Time: 13:36
 */

/**
 * Interface PrizeType
 */
interface PrizeType
{
    /** @var int money type */
    const TYPE_MONEY = 1;
    /** @var int bonuses type */
    const TYPE_BONUSES = 2;
    /** @var int product type */
    const TYPE_PRODUCTS = 3;

    /**
     * Get prize type
     * @return mixed
     */
    public function getType();

    /**
     * Get random prize
     * @return mixed
     */
    public function getValue();

    /**
     * Get is available current prize type
     * @return mixed
     */
    public function getIsAvailable();

    /**
     * Get name
     * @return string
     */
    public function getName();
}