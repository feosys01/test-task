<?php
namespace common\components\prizes\prizesTypes;

use common\components\prizes\PrizeType;
use common\models\mysql\ProductsRecord;

/**
 * Created by PhpStorm.
 * User: bohdan
 * Date: 01.12.18
 * Time: 13:27
 */

/**
 * Class ProductsType
 */
class ProductsType implements PrizeType
{
    /** @var  */
    private $value = null;

    /**
     * Get random prize
     * @return mixed
     */
    public function getValue()
    {
        if (is_null($this->value)) {
            // get rand product id
            $availableProducts = ProductsRecord::getAvailableProducts();

            if ($availableProducts && is_array($availableProducts)) {
                /** @var $productModel ProductsRecord */

                $randKey = array_rand($availableProducts);
                $productModel = $availableProducts[$randKey];

                $this->value = $productModel->id;
                $productModel->decCount();
            }
        }

        return $this->value;
    }

    /**
     * Get prize type
     * @return mixed
     */
    public function getType()
    {
        return self::TYPE_PRODUCTS;
    }

    /**
     * Get is available current prize type
     * @return mixed
     */
    public function getIsAvailable()
    {
        return ProductsRecord::getIsExistAvailableProducts();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'Product';
    }
}