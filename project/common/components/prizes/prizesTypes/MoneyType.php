<?php
namespace common\components\prizes\prizesTypes;

use common\components\prizes\PrizeType;

/**
 * Created by PhpStorm.
 * User: bohdan
 * Date: 01.12.18
 * Time: 13:27
 */

class MoneyType implements PrizeType
{
    /** @var int  */
    const MONEY_PRIZE_MIN = 100;
    /** @var int  */
    const MONEY_PRIZE_MAX = 2000;

    /** @var  */
    private $value = null;

    /**
     * Get random prize
     * @return mixed
     */
    public function getValue()
    {
        if (is_null($this->value)) {
            $this->value = rand(self::MONEY_PRIZE_MIN, self::MONEY_PRIZE_MAX);
        }

        return $this->value;
    }

    /**
     * Get prize type
     * @return mixed
     */
    public function getType()
    {
        return self::TYPE_MONEY;
    }

    /**
     * Get is available current prize type
     * @return mixed
     */
    public function getIsAvailable()
    {
        return true;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'Money';
    }
}