<?php

/**
 * Created by PhpStorm.
 * User: bohdan
 * Date: 01.12.18
 * Time: 15:10
 */

$this->title = 'My prises';

use yii\grid\GridView; ?>

<h1><?= $this->title ?></h1>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $model,
    'columns' => [
        'id',
        [
            'label' => 'name',
            'value' => function ($model) {
    /** @var $model \common\models\mysql\UsersPrisesRecord */
                return $model->getPrizeName();
            }
        ],
        [
            'label' => 'value',
            'value' => function ($model) {
                /** @var $model \common\models\mysql\UsersPrisesRecord */
                return $model->getPrizeValueText();
            }
        ],
        [
            'attribute' => 'is processed',
            'format' => 'raw',
            'value' => function($model){
                if($model->is_processed){
                    $result = 'yes';
                }else{
                    $result = 'no';
                }
                return $result;
            }
        ],
        ]
]); ?>