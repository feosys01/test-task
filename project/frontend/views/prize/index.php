<?php
/**
 * Created by PhpStorm.
 * User: bohdan
 * Date: 01.12.18
 * Time: 14:32
 */

use yii\helpers\Url; ?>


<div class="site-index">

    <div class="jumbotron">
        <h1>Get your free prize!</h1>
        <p><a class="btn btn-lg btn-success" href="<?= Url::toRoute(['prize/get-prize']) ?>">Get Random Prize</a></p>
    </div>
</div>

