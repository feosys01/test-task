<?php
/**
 * Created by PhpStorm.
 * User: bohdan
 * Date: 01.12.18
 * Time: 13:19
 */

namespace frontend\controllers;


use common\components\prizes\PrizeHelper;
use common\models\mysql\UsersPrizesRecord;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;

/**
 * Class ProfileController
 * @package frontend\controllers
 */
class ProfileController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionMyPrizes()
    {
        $model = new UsersPrizesRecord();
        $dataProvider = $model->search();

        return $this->render('my-prizes', ['model' => $model, 'dataProvider' => $dataProvider]);
    }
}