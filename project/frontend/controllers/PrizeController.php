<?php
/**
 * Created by PhpStorm.
 * User: bohdan
 * Date: 01.12.18
 * Time: 14:30
 */

namespace frontend\controllers;


use common\components\prizes\PrizeHelper;
use common\models\mysql\UsersPrizesRecord;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;

class PrizeController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @return string
     */
    public function actionGetPrize()
    {
        $prize = PrizeHelper::getRandomPrize();

        if (!empty($prize)) {
            $isPrizeAdded = UsersPrizesRecord::addPrize($prize, Yii::$app->user->id);

            if ($isPrizeAdded) {
                return $this->redirect(Url::toRoute(['prize/successful-page']));
            }
        }

        return $this->redirect(Url::toRoute(['prize/failure-page']));
    }

    /**
     * @return string
     */
    public function actionSuccessfulPage()
    {
        $userPrize = UsersPrizesRecord::getLastAddedPrize(Yii::$app->user->id);

        return $this->render('successful-page', ['userPrize' => $userPrize]);
    }

    /**
     * @return string
     */
    public function actionFailurePage()
    {
        return $this->render('failure-page');
    }
}